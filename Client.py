import socket

#-------------HELPER FUNCTIONS
def Recieve(s):
    data = s.recv(1024).decode('utf-8')
    print("Server: " + data)

def Send(s, message):
    s.send(message.encode('utf-8'))
#-----------------------------
    
def Main():
    host = input("Enter IP Address: ")
    port = int(input("Enter Port: "))

    s = socket.socket()
    s.connect((host, port))

    Recieve(s)

    Helo(s)
    Recieve(s)
    
    message = input("-> ")
    while message != 'q':
        Send(s, message)
        data = s.recv(1024).decode('utf-8')
        print("Server: " + data)
        if(data == "Server: 600\n See ya later!"):
            break
        message = input("-> ")
        
    s.close()

def Helo(s):
    name = str(s.getsockname())
    hello = str("HELO " + name)
    Send(s, hello)


if __name__ == '__main__':
    Main()
