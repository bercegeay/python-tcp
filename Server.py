#Socket Server

import socket
from http import HTTPStatus
from time import gmtime, strftime

#-------------HELPER FUNCTIONS
def Send(c, data):
    c.send(data.encode('utf-8'))

#Sends Connection Status 
def Connection_Status(c):
    status = (str(HTTPStatus.OK.value) + " CSP (Connection Succesful Protocol) version 1.0 ready")
    Send(c, status)
#-----------------------------
    
def Main():
          host = ''
          port = 8008 #Required port to use

          #create socket
          s = socket.socket()
          print("Socket Created.")

          #Bind socket to local host and port
          s.bind((host,port))
          print("Socket Binded")

          #Start listening on socket  
          s.listen(1)
          print("Socket Now Listening... ")
          c, addr = s.accept()
          print("Connection from: " + str(addr))

          Connection_Status(c)
          
          HELO(c)

          #now keep talking with the client
          while True:
              data = c.recv(1024).decode('utf-8')
              if not data:
                  break
              print("From connected user: " + data)
              #data = data.upper()

              if(data == "REQTIME"):
                  REQTIME(c)
              elif(data == "REQDATE"):
                  REQDATE(c)
              elif(data == "REQIP"):
                  REQIP(c, addr)
              elif(data[:4] == "ECHO"):
                  ECHO(c, data)
              elif(data == "BYE"):
                  BYE(c)
              else:
                  Error(c)
          c.close()


#---------------FUNCTIONS GO HERE
def HELO(c):
    try:
        recieve = c.recv(1024).decode('utf-8')
        hello = ("210 Hello " + recieve[3:] + ", pleased to meet you")
        Send(c, hello)
    except:
        error = "510 Sorry I can not service your request at this time.\n Connection closed."
    
def REQTIME(c):
    time = strftime("%H:%M:%S", gmtime())
    successful = ("220 " + time)
    error = "520 Sorry, time is not available right now"
    try:
        Send(c, successful)
    except:
        Send(c, error)
        
def REQDATE(c):
    date = strftime("%Y-%m-%d", gmtime())
    successful = ("230 " + date)
    error = "530 Sorry, date is not available right now"
    try:
        Send(c, successful)
    except:
        Send(c, error)

#echo back data from client
def ECHO(conn, data):
	 
        #Echo data back
        reply = ('Server: 240 ' + data[4:])
                #handle case if data does not get passed through

        if not data:
        	reply = 'Status 540: ' + 'Sorry, I can\'t seem to echo right now!' 
        Send(conn, reply)
      
def REQIP(conn, addr):

	#Receiving from client
        data = str(addr[0])
        #Echo data back
        reply = 'Server: 250 ' + data
        #handle case if data does not get passed through
        if not data:
        	reply = 'Status 550: ' + 'Sorry, I can\'t seem to get your IP address right now!' 
        Send(conn, reply)

def BYE(conn):
	reply = "Server: 600\n See ya later!"
	Send(conn, reply)

def Error(c):
    reply = "Server: Uknown Command."
    Send(c, reply)

if __name__ == '__main__':
    Main()


